def execute(){
	properties([
		parameters([
			string(name: 'Option', description: 'Option', defaultValue:'', trim: true),
			string(name: 'Tag', description: 'Tag', defaultValue:'', trim: true),
			string(name: 'Branch', description: 'Branch', defaultValue:'', trim: true),
		])
	])
	try{
		stage('Running second'){
			print("Done second")
			print("Option received: ${Option}" )
		}
	} catch(err){
		print("Error:" + err)
		throw err
	}
}
return this