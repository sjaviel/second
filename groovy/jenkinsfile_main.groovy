def execute_second(){
	try{
		second_result = build(job: 'second', parameters: [
			[$class: 'StringParameterValue', name: 'Option', value: "${Option}"],
			[$class: 'StringParameterValue', name: 'Tag', value: "${Tag}"],
			[$class: 'StringParameterValue', name: 'Branch', value: "${Branch}"],
		], propagate: false).result 
		if(second_result == 'FAILURE') {
			echo "second run failed"
		}
	} catch(err){
		print("Error: " + err)
	}
}


def execute(){
	properties([
		parameters([
			string(name: 'Option', description: 'Option', defaultValue:'', trim: true),
			string(name: 'Tag', description: 'Tag', defaultValue:'', trim: true),
			string(name: 'Branch', description: 'Branch', defaultValue:'', trim: true),
		])
	])
	try{
		stage('Running main'){
			print("Done main")
		}
		stage('Running second call'){
			execute_second()
		}
	} catch(err){
		print("Error:" + err)
		throw err
	}
}
return this